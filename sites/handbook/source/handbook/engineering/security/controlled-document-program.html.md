---
layout: handbook-page-toc
title: "Controlled Document Program"
description: "GitLab deploys control activities through policies and standards that establish what is expected and procedures that put policies and standards into action."
---
 
## On this page
{:.no_toc .hidden-md .hidden-lg}
 
- TOC
{:toc .hidden-md .hidden-lg}
 
# Controlled Document Annual Review Program
 
## Purpose
 
The Controlled Documents Annual Review Program focuses on the annual review of Controlled Documents to ensure they are meeting [procedural expectations](https://about.gitlab.com/handbook/engineering/security/controlled-document-procedure.html). The Controlled Document Procedure is in place to ensure that there is consistency in developing and maintaining controlled documents at GitLab according to legal and regulatory requirements. 

 
## Scope
 
1. Controlled Documents: Formal policies, standards and procedures.
 
Goal: 100% of controlled documents reviewed and approved annually.
 
#### List of Controlled Documents:
 
<details markdown="1">
 
| Document Name | Description | URL | Code Owners |                                        
| :----: | :--------------------------------------: | :----: |:----:  |                                                                                    
| Acceptable Use Policy | This policy specifies requirements related to the use of GitLab computing resources and data assets by GitLab team members so as to protect our customers, team members, contractors, company, and other partners from harm caused by both deliberate and inadvertent misuse. | https://about.gitlab.com/handbook/people-group/acceptable-use-policy/ | Security, Legal and PeopleOps |                                                       
| Access Management Policy | *Procedure | https://about.gitlab.com/handbook/engineering/security/access-management-policy.html | Security Assurance Management |
| Access Review Guidelines | This policy defines the importance of the User access review process as an important control activity required for internal and external IT audits, helping to minimize threats, and provide assurance of who has access to what. | https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/access-reviews.html | Security Compliance Team |
| Backup Policy | This policy is to document that our production databases are taken every 24 hours with continuous incremental data (at 60 sec intervals). | https://about.gitlab.com/handbook/engineering/infrastructure/production/#backups | Infrastructure Management Team |                                         
| Backup Recovery Testing | This project implements a backup testing pipeline. It's purpose is to detect whether or not the backup is actually restorable and in good shape. After all, what good is a backup that cannot be restored?  This is implemented in a CI pipeline that we kick off daily on a schedule. It can also be triggered manually at any time (parallel pipelines are supported). | https://gitlab.com/gitlab-com/gl-infra/gitlab-restore/postgres-gprd/blob/master/README.md | Infrastructure Management Team |
| Business Continuity Plan | A business continuity plan is an overall organizational program for achieving continuity of operations for business functions. Continuity planning addresses both information system restoration and implementation of alternative business processes when systems are compromised. | https://about.gitlab.com/handbook/business-technology/gitlab-business-continuity-plan/ | Information Technology Team |
| Change Management Policy | This policy is to manage changes in the operational environment with the aim of doing so (in order of highest to lowest priority) safely, effectively and efficiently. | https://about.gitlab.com/handbook/engineering/infrastructure/change-management/ | Infrastructure Management Team |                                                
| Controlled Documents Program | Deploying control activities through policies and standards that establish what is expected and procedures that put policies and standards into action ensuring there is consistency in developing and maintaining controlled documents at GitLab utilizing a hierarchal approach for managing legal and regulatory requirements. | https://about.gitlab.com/handbook/engineering/security/controlled-document-procedure.html | Security Assurance Management |
| Data Classification Policy | This policy defines data categories and provides a matrix of security and privacy controls for the purposes of determining the level of protection to be applied to GitLab data throughout its lifecycle. | https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html | Security Assurance Management |
| Data Protection Impact Assessment (DPIA) Policy | This policy ensures that our use of personal data is fully understood, that risks to the rights and freedoms of individuals resulting from the processing of personal data are carefully examined and that all appropriate measures are put in place to protect these rights throughout the lifecycle of the processing. DPIAs, in conjunction with the associated forms and guidance, should be used to ensure that our obligations and policies in this area are met. | https://about.gitlab.com/handbook/legal/privacy/dpia-policy/| Security Management |                             
| Data Team Policy and Standards | This policy documents how the data team delivers results that matter securing our data. | https://about.gitlab.com/handbook/business-ops/data-team/ | Data Team Management |                                                                
| Database Disaster Recovery Policy | This policy documents our disaster recovery for databases. | https://about.gitlab.com/handbook/engineering/infrastructure/database/disaster_recovery.html | Infrastructure Management Team |                         
| Disaster Recovery | | https://gitlab.com/gitlab-com/gl-infra/readiness/-/blob/master/library/disaster-recovery/index.md |  Infrastructure Management Team |
| Encryption Policy | This policy documents the encryption process in which data is securely encoded at rest and in transit to remain hidden from or inaccessible to unauthorized users to better protect private, proprietary and sensitive data and enhance the security of communication between client applications and servers. | https://about.gitlab.com/handbook/engineering/security/vulnerability_management/encryption-policy.html | Security Assurance Management |                            
| GCF Security Control Lifecycle | | https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/security-control-lifecycle.html | Security Compliance Management |
| GitLab Password Policy | This policy documents the process of constructing secure passwords and ensuring proper password management to protect GitLab information systems and other resources from unauthorized use based, in part, on the recommendations by NIST 800-63B. | https://about.gitlab.com/handbook/security/#gitlab-password-policy-guidelines | Security Assurance Management |                                                         
| GitLab Terms of Service | The page documents the terms of service when using GitLab | https://about.gitlab.com/terms/ | GitLab Legal |
| Information Security Management System (ISMS) | This policy documents the boundaries and objectives of GitLab's ISMS | https://about.gitlab.com/handbook/engineering/security/ISMS.html | Security Assurance Management |
| IT Help Team Policy and Standards | This policy documents IT Support responsibilities for onboarding and managing company assets. | https://about.gitlab.com/handbook/business-ops/employee-enablement/it-help/ | Business Technology Management |
| IT Ops Policy and Standards | This policy documents IT Operations responsibilities for onboarding and managing company assets. | https://about.gitlab.com/handbook/business-ops/employee-enablement/it-ops-team/ | Business Technology Management |
| Off-boarding Policy Guidelines | This document is an off-boarding step by step process that covers all the steps necessary to successfully part ways with an employee following their resignation or termination. When done well, a clear offboarding process ensures a smooth transition for both the company and the departing employee. | https://about.gitlab.com/handbook/offboarding/offboarding_guidelines/ | People Operations Management |                                                             
| Production Architecture | The GitLab.com core infrastructure is primarily hosted in Google Cloud Platform's (GCP) us-east1 region (see Regions and Zones)—and we use GCP iconography in our diagrams to represent GCP resources. We do have dependencies on other cloud providers for separate functions. Some of the dependencies are legacy fragments from our migration from Azure, and others are deliberate to separate concerns in the event of cloud provider service disruption.   This document does not cover servers that are not integral to the public facing operations of GitLab.com. | https://about.gitlab.com/handbook/engineering/infrastructure/production/architecture/ | Infrastructure Management Team |     
| Records Retention & Disposal Policy | Thi GitLab records retention and disposal standard lists the specific retention and secure disposal requirements for critical GitLab records. | https://about.gitlab.com/handbook/engineering/security/records-retention-deletion.html | Security Risk Management|     
| Risk Management Policy | This policy documents the Information Security Risk Management Program for performing risk analysis of information resources that store, process or transmit an organization's data.  The purpose of the Security Operational Risk Management (“StORM”) program at GitLab is to identify, track, and treat security operational risks in support of GitLab's organization-wide objectives. The Security Risk team utlizes these procedures to ensure that security risks that may impact GitLab's ability to achieve its customer commitments and operational objectives are effectively identified and treated. | https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/guidance/RM.1.05_risk_management.html | Security Risk Management |
| Security Compliance Observation Management Procedure | | https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/observation-management.html | Security Compliance Management |
| Security Incident Communications Plan Policy | This policy documents the communication response plan to map out the who, what, when, and how of GitLab in notifying and engaging with internal stakeholders and external customers on security incidents. This plan of action covers the strategy and approach for security events which have a ‘high’ or greater impact as outlined in GitLab’s risk scoring matrix. | https://about.gitlab.com/handbook/engineering/security/security-operations/sirt/security-incident-communication-plan.html | Security Management |
| Security Incident Response Guide | This Policy documents the responsibilities of all GitLab team members when responding to or reporting security incidents. | https://about.gitlab.com/handbook/engineering/security/security-operations/sirt/sec-incident-response.html | Security Management |
| Security Operations On-Call Guide (Major Incidents) | This policy documents how the Security Operations Team (SecOps) is collectively on-call 24/7/365, split into 12-hour shifts Monday to Friday and 48-hour coverage Saturday and Sunday. | https://about.gitlab.com/handbook/engineering/security/secops-oncall.html#major-incident-response-workflow | Security Assurance Management |
| Third Party Vendor Security Review Policy | This policy documents how security reviews are performed on new and renewing third party vendors that are requested through the procurement process and minimizing the risk associated with third party applications and services. | https://about.gitlab.com/handbook/engineering/security/security-assurance/risk-field-security/third-party-risk-management.html | Security Risk Management |
| Vulnerability Management Policy Overview | This policy documents the recurring process of identifying, classifying, prioritizing, mitigating, and remediating vulnerabilities. This overview will focus on infrastructure vulnerabilities and the operational vulnerability management process designed to provide insight into our environments, leverage GitLab for vulnerability workflows, promote healthy patch management among other preventative best-practices, and remediate risk; all with the end goal to better secure our environments. | https://about.gitlab.com/handbook/engineering/security/vulnerability_management/#vulnerability-management-overview | Security Assurance Management |
 
</details>
 
 
## Roles & Responsibilities:
 
| Role  | Responsibility |
|-----------|-----------|
| Security Governance Team | Responsible for maintaining the program and facilitating annual controlled documents review |
| Security Compliance Team | Responsible for testing Security Policies and supporting standards and procedures as part of ongoing continuous control monitoring |
| Security Assurance Management | Responsible for approving changes to this program |
| Code Owners | Responsible for defining, implementing and maintaining controlled documents for their programs |
| Everyone |  Everyone is welcomed and encouraged to create or suggest changes to controlled documents at any time.
 
## Procedure
 
### Annual Review
Controlled documents are required to be reviewed and approved on a minimum of an annual basis and may be updated ad hoc as required by business operations.

The Security Governance team will initiate and track the annual review via a GitLab Epic. Issues will be opened for each department and assigned to the relevant Code Owners with a list of in scope docuemnts and instructions. Merge Requests will be opened in cases where changes are required and tracked via the same issue.

#### Annual Review Epics
- [FY23 Controlled Document Review](https://gitlab.com/groups/gitlab-com/gl-security/security-assurance/governance/-/epics/7)

### SSOT Maintenance
When a new controlled document is identified, an MR should be opened and assigned to Security Governance to add the document to the List of Controlled Documents. 

The [Controlled Documents Template](https://gitlab.com/gitlab-com/gl-security/security-assurance/governance/security-governance/-/blob/master/runbooks/controlled%20documents%20template.md) should be applied at this time if not already done. 
 
## Exceptions
Exceptions to this program will be tracked as per the [Information Security Policy Exception Management Process](/handbook/engineering/security/#information-security-policy-exception-management-process).
 
## References
* Parent Policy: [Information Security Policy](/handbook/engineering/security/)
* [Controlled Document Procedure](https://about.gitlab.com/handbook/engineering/security/controlled-document-procedure.html)
* [SCF Compliance Controls](/handbook/engineering/security/security-assurance/security-compliance/guidance/compliance.html)
* [Data Classification Standard](/handbook/engineering/security/data-classification-standard.html)
